<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <link rel="icon" type="image/png" href="{{ asset('public/favicon.ico') }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'IDRD - GRAN RETO GUERREROS') }}</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

    <!--    Bootstrap, Fonts and icons     -->

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link href="{{ asset('public/css/app.css?v=2.1.0') }}" rel="stylesheet">
    <link href="{{ asset('public/js/bootstrap-datepicker/css/bootstrap-datepicker.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <link href="https://unpkg.com/bootstrap-table@1.14.1/dist/bootstrap-table.min.css" rel="stylesheet">

    <link href="https://mdbootstrap.com/docs/jquery/javascript/carousel/">


  <!--  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link href="{{ asset('public/css/app.css?v=2.1.0') }}" rel="stylesheet">
    <link href="{{ asset('public/js/bootstrap-datepicker/css/bootstrap-datepicker.css') }}" rel="stylesheet">-->

</head>
<body class="contact-page sidebar-collapse">

<!--    navbar come here          -->
@include('sections._nav-bar')
<!-- end navbar  -->

<div class="wrapper" id="app">
    @yield('content')
</div>

<!-- Footer come here -->
@include('sections._footer')
<!--   end footer -->


</body>
<!--  Plugins -->


<script src="{{ asset('public/js/manifest.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/js/vendor.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/js/moment.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/js/app.js') }}" type="text/javascript"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>




<script src="{{ asset('public/js/jquery.repeater.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/js/repetidor.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/js/maxLength.min.js') }}" type="text/javascript"></script>



<script src="https://unpkg.com/bootstrap-table@1.14.1/dist/bootstrap-table.min.js"></script>

<script src="https://unpkg.com/bootstrap-table@1.14.1/dist/bootstrap-table-locale-all.min.js"></script>

<script src="https://unpkg.com/tableexport.jquery.plugin/libs/jsPDF/jspdf.min.js"></script>

<script src="https://unpkg.com/tableexport.jquery.plugin/libs/jsPDF-AutoTable/jspdf.plugin.autotable.js"></script>

<script src="https://unpkg.com/bootstrap-table@1.14.1/dist/bootstrap-table.min.js"></script>

<script src="https://unpkg.com/bootstrap-table@1.14.1/dist/extensions/export/bootstrap-table-export.min.js"></script>

<script src="https://unpkg.com/tableexport.jquery.plugin/tableExport.min.js"></script>



<script src="{{ asset('public/js/tabla.js') }}"></script>

</html>