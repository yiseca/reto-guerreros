@extends('master')                              

@section('content')

<div class="section section-image section-login" style="background-image: url('{{ asset('public/images/stak2.jpg') }}');">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 ml-auto mr-auto">
                            <h1 class="title text-center">Listado Inscritos <br> EL GRAN RETO DE GUERREROS</h1><br>
                        </div>
                    </div>
                </div>
            </div>

<div class="section">
<div class="container content-center">
<h4 class="text-info">Inscritos</h4>
        <br>
    <div class="row">

        <div class="col-md-12">
                <div class="fresh-table toolbar-color-blue" align="center">
                    {!! $tabla !!}
                </div>
            </div>
    </div>
</div>
    
</div>


@stop
