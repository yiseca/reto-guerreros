<!-- Background -->
<section class="section section-shaped section-lg ">
    <div id="contact" class="shape bg-gradient-info shape-style-1 shape-default">
    </div>
    <div class="container pt-lg pb-300">
        <div class="row text-center justify-content-center">
            <div class="col-lg-12">
                <h5 class="display-5 text-info">UTILICE EL SIGUIENTE FORMULARIO PARA INSCRIBIRSE EN EL GRAN RETO DE GUERREROS</h5>
            </div>
        </div>
    </div>
    <!-- SVG separator -->
</section>


<!-- Form -->
<form method="POST" action="insertar" class="repeater" id="form_gen">
    <section class="section section-lg pt-lg-0 section-contact-us" id="formulario">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="container">
            <div class="row justify-content-center mt--300">
                <div class="col-lg-12">
                    <div class="card bg-gradient-secondary shadow">
                        <div class="card-body p-lg-5">
                            <p class="mt-0">Todos los campos con * son obligatorios.</p>
                            <br>
                            <div class="row">

                                <div class="col-md-12">
                                    <div id="error_message"></div>
                                </div>

                                <!--tipo y número de documento-->
                                <div class="col-md-12">
                                    <label style="font-size: 0.9em; font-weight:600; line-height: 1.5em"><span
                                                style="color: red;font-size: 16px;text-transform: capitalize;color:red">*</span>&nbsp;&nbsp;NOMBRE
                                        DE EQUIPO</label>
                                    <input required type="text" class="form-control" id="equipo" name="equipo">
                                    <br>
                                </div>


                                <!--nombre y apellido completo-->
                                <div class="col-md-6">
                                    <label style="font-size: 0.9em; font-weight:600; line-height: 1.5em"><span
                                                style="color: red;font-size: 16px;text-transform: capitalize;color:red">*</span>&nbsp;&nbsp;SELECCIONE
                                        CATEGORÍA</label>
                                    <select required name="categoria" id="categoria" class="form-control">
                                        <option value="">Seleccione</option>
                                        <option value="1">MIXTA</option>
                                    </select>
                                </div>
                                <div class="col-md-6">

                                    <label style="font-size: 0.9em; font-weight:600; line-height: 1.5em"><span
                                                style="color: red;font-size: 16px;text-transform: capitalize;color:red">*</span>&nbsp;&nbsp;SELECCIONE
                                        HORARIO </label>
                                    <select required name="horario" id="horario" class="form-control">
                                        <option value="">Seleccione</option>
                                        @foreach ($horas as $hora)
                                            <option value="{{ $hora->id }}">{{ $hora->hora}}</option>
                                        @endforeach
                                    </select>
                                    <br>
                                </div>


                                <br>
                                <br>

                                <fieldset class="form-group">
                                    <button class="btn btn-info btn-block" data-repeater-create type="button">Agregar
                                        Participante
                                    </button>
                                </fieldset>

                                <hr>
                                <br>

                                <div class="col-md-12 repeater" data-limit="4">
                                    <div data-repeater-list="autores" class="col-md-12">

                                        <div data-repeater-item class="row">

                                            <div class="row">


                                                <!--nombre y apellido completo-->

                                                <div class="col-md-6">

                                                    <label style="font-size: 0.9em; font-weight:600; line-height: 1.5em"><span
                                                                style="color: red;font-size: 20px;text-transform: capitalize;color:red">*</span>&nbsp;&nbsp;NOMBRES
                                                        COMPLETOS</label>

                                                    <input required type="text" class="form-control" id="nombre"
                                                           name="participantes[nombres][]">

                                                </div>

                                                <div class="col-md-6">

                                                    <label style="font-size: 0.9em; font-weight:600; line-height: 1.5em"><span
                                                                style="color: red;font-size: 20px;text-transform: capitalize;color:red">*</span>&nbsp;&nbsp;APELLIDOS
                                                        COMPLETOS</label>

                                                    <input required type="text" class="form-control" id="apellido"
                                                           name="participantes[apellidos][]">

                                                    <br>

                                                </div>

                                                <!--tipo y número de documento-->

                                                <div class="col-md-6">

                                                    <label style="font-size: 0.9em; font-weight:600; line-height: 1.5em"><span
                                                                style="color: red;font-size: 20px;text-transform: capitalize;color:red">*</span>&nbsp;&nbsp;TIPO
                                                        DE DOCUMENTO</label>

                                                    <select required name="participantes[tipo_documento][]"
                                                            id="tipo_documento" class="form-control">

                                                        <option value=" ">Seleccione</option>

                                                        <option value="Cedula de Ciudadanía">Cedula de Ciudadanía
                                                        </option>

                                                        <option value="Cedula de Extrangería">Cedula de Extrangería
                                                        </option>

                                                        <option value="Tarjeta de Identidad">Tarjeta de Identidad
                                                        </option>

                                                        <option value="Pasaporte">Pasaporte</option>

                                                    </select>

                                                </div>

                                                <div class="col-md-6">

                                                    <label style="font-size: 0.9em; font-weight:600; line-height: 1.5em"><span
                                                                style="color: red;font-size: 20px;text-transform: capitalize;color:red">*</span>&nbsp;&nbsp;NÚMERO
                                                        DE DOCUMENTO</label>

                                                    <input required type="text" class="form-control" id="cedula"
                                                           name="cedula">

                                                    <br>

                                                </div>


                                                <!--fecha de nacimiento y edad-->

                                                <div class="col-md-6">

                                                    <label style="font-size: 0.9em; font-weight:600; line-height: 1.5em"><span
                                                                style="color: red;font-size: 20px;text-transform: capitalize;color:red">*</span>&nbsp;&nbsp;FECHA
                                                        DE NACIMIENTO</label>

                                                    <input type='date' min="1950-01-01" max="{{ \Carbon\Carbon::now()->subYears(18)->format('Y-m-d')  }}"
                                                           autocomplete="off" class="form-control datetimepicker"
                                                           data-provide="datepicker" data-date-language="es"
                                                           data-date-format="yyyy-mm-dd"
                                                           name="participantes[fecha_nacimiento][]"
                                                           id="fecha_nacimiento"/>

                                                </div>

                                                <div class="col-md-6">

                                                    <label style="font-size: 0.9em; font-weight:600; line-height: 1.5em"><span
                                                                style="color: red;font-size: 20px;text-transform: capitalize;color:red">*</span>&nbsp;&nbsp;EPS</label>

                                                    <input required type="text" class="form-control" id="eps"
                                                           name="participantes[eps][]">

                                                    <br>

                                                </div>


                                                <!--género y patios-->

                                                <div class="col-md-6">

                                                    <label style="font-size: 0.9em; font-weight:600; line-height: 1.5em"><span
                                                                style="color: red;font-size: 20px;text-transform: capitalize;color:red">*</span>&nbsp;&nbsp;GÉNERO</label>

                                                    <select name="participantes[genero][]" id="genero"
                                                            class="form-control">

                                                        <option value=" ">Seleccione</option>

                                                        <option value="Masculino">Masculino</option>

                                                        <option value="Femenino" selected>Femenino</option>

                                                    </select>

                                                </div>


                                                <div class="col-md-6">

                                                    <label style="font-size: 0.9em; font-weight:600; line-height: 1.5em"><span
                                                                style="color: red;font-size: 20px;text-transform: capitalize;color:red">*</span>&nbsp;&nbsp;CORREO
                                                        ELECTRÓNICO</label>

                                                    <input required type="email" class="form-control" id="mail"
                                                           name="participantes[mail][]">

                                                    <br>

                                                </div>


                                                <!-- celular y eps-->

                                                <div class="col-md-6">

                                                    <label style="font-size: 0.9em; font-weight:600; line-height: 1.5em"><span
                                                                style="color: red;font-size: 20px;text-transform: capitalize;color:red">*</span>&nbsp;&nbsp;CELULAR</label>

                                                    <input required type="number" class="form-control" id="celular"
                                                           name="participantes[celular][]">

                                                </div>


                                                <!--teléfono de contacto-->

                                                <div class="col-md-6">

                                                    <label style="font-size: 0.9em; font-weight:600; line-height: 1.5em"><span
                                                                style="color: red;font-size: 20px;text-transform: capitalize;color:red">*</span>&nbsp;&nbsp;NOMBRE
                                                        DE CONTACTO DE EMERGENCIA</label>

                                                    <input required type="text" class="form-control"
                                                           id="nombre_contacto" name="participantes[nombre_contacto][]">

                                                    <br>

                                                </div>

                                                <div class="col-md-6">

                                                    <label style="font-size: 0.9em; font-weight:600; line-height: 1.5em"><span
                                                                style="color: red;font-size: 20px;text-transform: capitalize;color:red">*</span>&nbsp;&nbsp;TELÉFONO
                                                        DE CONTACTO DE EMERGENCIA</label>

                                                    <input required type="number" class="form-control"
                                                           id="numero_contacto" name="participantes[numero_contacto][]">

                                                    <br>

                                                </div>


                                            </div>

                                            <div class="col-md-2">
                                                <br><input data-repeater-delete type="button" class="btn btn-danger"
                                                           value="Quitar"/>
                                                <br><br>
                                                <hr>
                                            </div>

                                            <hr>

                                        </div>


                                    </div>
                                </div>

                                <hr>
                                <br><br>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <a href="javascript:;" data-toggle="modal" data-target="#modal-default"
                                           class="btn btn-link text-info">Cláusula de exoneración y consentimiento
                                            informado</a>
                                    </div>
                                    <br>
                                    <div class="custom-control custom-checkbox mb-3">
                                        <input class="custom-control-input" required id="customCheck1" type="checkbox">
                                        <label class="custom-control-label" for="customCheck1">
                                            <span>Acepto Términos de Incripción</span>
                                        </label>
                                    </div>
                                    <br>
                                    <div>
                                        <button type="submit" id="btn_send" class="btn btn-info btn-round btn-block btn-lg"
                                                value="enviar">
                                            Enviar
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</form>

<!-- Modal -->
<div class="modal fade" id="modal-default" tabindex="-1" role="dialog" aria-labelledby="modal-default"
     aria-hidden="true">
    <div class="modal-dialog modal- modal-dialog-centered modal-" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title" id="modal-title-default">Cláusula de exoneración y consentimiento informado</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <textarea class="form-control form-control-alternative" readonly="readonly" name="name" rows="10"
                          cols="80" style="text-align: justify;">Los arriba firmantes inscritos al programa deportivo, certifican que hemos sido informados sobre el objetivo, los beneficios y posibles riesgos que pudieran surgir durante el desarrollo del programa y/o actividades, por lo tanto, asumimos de manera directa y exclusiva la responsabilidad por cualquier eventualidad, situación y/o accidente que se pueda presentar durante el mismo. En ese sentido, exoneramos al Instituto Distrital de Recreación y Deporte IDRD, a REAL TIME GROUP S.A.S y a los colaboradores terceros encargados en el desarrollo del ejercicio, de cualquier responsabilidad que se derive de la participación o práctica del programa o actividades deportivas que vayan más allá de la notificación de la EPS o Sisbén, según sea el caso.  Así mismo, autorizo al IDRD para el manejo y uso adecuado de datos personales, fotografías y vídeos dentro del marco al derecho a la información y la autodeterminación informativa o protección de datos personales y publicar en la página web del Instituto Distrital de Recreación y Deporte IDRD y demás medios de comunicación radiales y de televisión lo que se considere necesarios; en el marco de la participación del programa. Con mi firma doy constancia de que leí, entendí y acepto el contenido del presente documento.</textarea>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link  ml-auto" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

