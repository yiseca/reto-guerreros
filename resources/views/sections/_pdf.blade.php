<!-- Background -->
<section class="section section-shaped section-lg ">
    <div id="download" class="shape bg-gradient-info shape-style-1 shape-default">
    </div>
    <div class="container pt-lg pb-300">
        <div class="row text-center justify-content-center">
            <div class="col-lg-12">
                <h5 class="display-5 text-info">UTILICE EL SIGUIENTE FORMULARIO PARA DESCARGAR EL
                    COMPROBANTE DE INSCRIPCION</h5>
            </div>
        </div>
    </div>
    <!-- SVG separator -->
</section>
<!-- Form -->
<form method="POST" action="comprobante" class="repeater" id="form_gen" target="_blank" >
    <section class="section section-lg pt-lg-0 section-contact-us">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="container">
            <div class="row justify-content-center mt--300">
                <div class="col-lg-12">
                    <div class="card bg-gradient-secondary shadow">
                        <div class="card-body p-lg-5">
                            <div class="row">

                                <div class="col-md-12">
                                    <label style="font-size: 0.9em; font-weight:600; line-height: 1.5em"><span style="color: red;font-size: 16px;text-transform: capitalize;color:red">*</span>&nbsp;&nbsp;NÚMERO
                                        DE DOCUMENTO</label>
                                    <input required type="text" class="form-control" id="cedula" name="cedula">
                                    <br>
                                </div>

                                <div>
                                    <button type="submit" class="btn btn-info btn-round btn-block btn-lg"
                                            value="enviar">Descargar
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</form>

