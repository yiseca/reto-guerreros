<div id="news" class="section section-dark text-center">
    <div class="container">
        <div class="row">
            <div class="row">
                <div class="col-md-12 ml-auto mr-auto">
                    <h2 class="title">Las fechas límite para la presentación de resúmenes son:</h2><br/>
                </div>
            </div>
            <div class="col-lg-3 col-md-3">
                <h2 class="description"><i class="nc-icon nc-calendar-60"></i></h2><br/>
                <p class="description" style="font-size: 18px">
                    08 de junio al 30 de Junio
                    <br>
                    <small>Recepción de resúmenes.</small>
                </p>
            </div>
            <div class="col-lg-3 col-md-3">
                <h2 class="description"><i class="nc-icon nc-calendar-60"></i></h2><br/>
                <p class="description" style="font-size: 18px">
                    1 de julio al 15 de Julio
                    <br>
                    <small>Evaluación y clasificación de los resúmenes por medio del comité científico del evento.</small>
                </p>
            </div>
            <div class="col-lg-3 col-md-3">
                <h2 class="description"><i class="nc-icon nc-calendar-60"></i></h2><br/>
                <p class="description" style="font-size: 18px">
                    16 de julio al 31 de Julio
                    <br>
                    <small>Envío de la aceptación o rechazo de los resúmenes a las instituciones educativas.</small>
                </p>
            </div>
            <div class="col-lg-3 col-md-3">
                <h2 class="description"><i class="nc-icon nc-calendar-60"></i></h2><br/>
                <p class="description" style="font-size: 18px">
                    1 de agosto al 15 agosto
                    <br>
                    <small>Envío de la programación a las instituciones educativas.</small>
                </p>
            </div>
            <div class="col-lg-12 col-md-12">
                <h2 class="description"><i class="nc-icon nc-calendar-60"></i></h2><br/>
                <p class="description" style="font-size: 18px">
                    12 de octubre
                    <br>
                    <small>Presentación de resúmenes en el marco del evento.</small>
                </p>
            </div>
        </div>
    </div>
</div>