<nav class="navbar navbar-expand-md fixed-top navbar-transparent" color-on-scroll="500">
    <div class="container">
        <div class="navbar-translate">
            <button class="navbar-toggler navbar-toggler-right navbar-burger" type="button" data-toggle="collapse" data-target="#navbarToggler" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-bar"></span>
                <span class="navbar-toggler-bar"></span>
                <span class="navbar-toggler-bar"></span>
            </button>
            <a class="navbar-brand" href="https://www.idrd.gov.co" style="font-size: 20px;">IDRD</a>
        </div>
        <div class="collapse navbar-collapse" id="navbarToggler">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a href="welcome#contact" class="nav-link top-menu" style="font-size: 20px">Inscríbete aquí</a>
                </li>
                <li class="nav-item">
                    <a href="welcome#top" class="nav-link top-menu" style="font-size: 20px"> Inicio</a>
                </li>
                <!--<li class="nav-item">
                    <a href="welcome#download" class="nav-link top-menu" style="font-size: 20px">Descargar Comprobante</a>
                </li>-->
                
            </ul>
        </div>
    </div>
</nav>
