<div id="description" class="section text-center">
    <div class="container">
        <h4 class="text-info">INSCRIPCIONES FESTIVAL DE VERANO 2019</h4>
        <br>
        <div class="row">
            <div class="col-md-12 ml-auto mr-auto">
                <p align="justify" style="font-size: 18px">
                  El GRAN RETO DE GUERREROS es una experiencia única llena de diversión que pone a prueba tus límites físicos en una carrera competitiva en equipos sobre una pista de obstáculos.
                  Queremos invitar a los participantes del Festival de Verano de Bogotá 2019 a vivir momentos de emoción y diversión que te pondrán a prueba junto a tu equipo en un verdadero reto de Guerreros. 
 
                </p><br>

            </div>
        </div>
        <br/>
     <h4 class="text-info">DETALLE DEL DESARROLLO DEL EVENTO</h4><br>

                <ul style="text-align: left; font-size: 18px">

                      <li><strong class="text-info">JORNADA 1 – Clasificación Equipos Mixtos</strong>
                           <ul>
                               <li><strong class="text-info">Día:</strong> 03/08/2019</li>
                               <li><strong class="text-info">Lugar:</strong>Parque Metropolitano Simón Bolívar - TALUD</li>
                               <li><strong class="text-info">Horario:</strong> 8:00 a.m. a 5:00 p.m.</li>
                               <li><strong class="text-info">Total máximo inscritos: </strong>300 equipos (1200 personas)</li>                      
                           </ul><br>
                      </li>
                      
                       <li><strong class="text-info">JORNADA 2 – Eliminatorias Equipos Mixtos</strong>
                           <ul>
                               <li><strong class="text-info">Día:</strong> 04/08/2019</li>
                               <li><strong class="text-info">Lugar:</strong>Parque Metropolitano Simón Bolívar - TALUD</li>
                               <li><strong class="text-info">Horario:</strong>8:00 a.m. a 3:00 p.m.</li>
                                                  
                           </ul><br>
                      </li>
                       <li><strong class="text-info">FINAL</strong>
                           <ul>
                               <li><strong class="text-info">Día:</strong> 04/08/2019</li>
                               <li><strong class="text-info">Lugar:</strong>Parque Metropolitano Simón Bolívar</li>
                               <li><strong class="text-info">Horario:</strong>4:00 p.m. a 5:00 p.m.</li>
                               <li><strong class="text-info">Total equipos Finalistas: : </strong>30 equipos</li>                   
                           </ul><br>
                      </li>
                      <li><strong class="text-info">REQUISITOS</strong>
                           <ul>
                               <li>Ser mayor de 18 años</li>
                               <li>Diligenciar y firmar el formato de exoneración de responsabilidad</li>
                               <li>Diligenciar y firmar la autorización de manejo de base de datos</li>
                               <li>Haber realizado pre inscripción en la página del IDRD</li> 
                               <li>Tomar foto de Ingreso previo al inicio del evento</li>  
                               <li>Presentarse 30 minutos antes del inicio programado de la prueba</li>                     
                           </ul>
                      </li>
                      
                 </ul>
                 

    </div>
</div>