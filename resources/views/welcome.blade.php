@extends('master')                              

@section('content')
    @include('sections._landing')

    <div class="main">
@include('sections._description')

<!--@include('sections._news')-->

<br><br>
      <!--  <div class="section">
            <div id="images">
                <div class="container">
                    <div class="tim-title">
                        <h3>Revistas</h3>
                    </div>
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-12 content-center">
                            <h4 class="images-title">Revista movimiento científico</h4>
                            <img src="{{ asset('public/images/revista.png') }}" class="img-thumbnail img-responsive" alt="Revista movimiento científico">
                            <p class="text-center">
                                <small>
                                    Todos los resúmenes las memorias del 4° Seminario Nacional y 1° Internacional de Actividad Física 2018, serán aceptados y publicados
                                    en el suplemento de la revista Movimiento Científico de la Corporación Universitaria Iberoamericana, indexada por Dialnet, latindex, Miar.
                                    Para más información de la revista hacer clic en la imagen.
                                </small>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>-->

        {{-- @include('sections._tabs')--}}

        @include('sections._contact')

    </div>
@stop
