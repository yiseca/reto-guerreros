<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
</head>
<style>
    td {
        border-bottom: 1px solid #ddd;
        margin: 5px;
    }
</style>
<body>
<div>
    <div style="float: left;">
        <p>FESTIVAL DE VERANO<br/>
            IDRD<br/>
            2019
        </p>
    </div>
    <div style="float: right">
        <img style="width: 250px" src="{{ asset('https://www.idrd.gov.co/SIM/Recreacionv2/public/Img/IDRD.JPG') }}">
    </div>
</div>
<div>
    <div style="text-align: center; padding-top: 130px;">
        <h1>"CAPITAL CRIT – La corona de la Calle"</h1>
    </div>
</div>
<div>
    <div style="float: right">

    </div>
    <div style="text-align: left">
        <p><strong >Día del evento:</strong> Agosto 4 de 2019.<br/>
            <strong>Hora de Inicio:</strong> 06:00 am.<br/>
            <strong>Lugar:</strong> Parque Tunal – Sobre el circuito frente la Administración del parque.<br/>
        </p>
    </div>
</div>
<div>
    <table cellspacing="0">
        <thead style="background-color: #eeeeee; border: none;">
        <tr>
            <th width="80px" height="35px" style="margin: 5px">ID</th>
            <th width="180px">DOCUMENTO</th>
            <th width="280px">NOMBRE</th>
            <th width="160px">CATEGORIA</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td height="45px">{{ isset( $formulario->id ) ? $formulario->id : '' }}</td>
            <td>{{ isset( $formulario->cedula ) ? $formulario->cedula : '' }}</td>
            <td>{{ isset( $formulario->nombre, $formulario->apellido ) ? "{$formulario->nombre} {$formulario->apellido}" : '' }}</td>
            <td>{{ isset( $categoria->nombre_categoria) ? "{$categoria->nombre_categoria}" : '' }}</td>
        </tr>
        </tbody>
    </table>
</div>
</body>
</html>