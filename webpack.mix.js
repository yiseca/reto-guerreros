const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .extract([
        'jquery',
        'jquery-ui',
        'popper.js',
        'bootstrap',
        'bootstrap-switch',
        'nouislider',
        'moment',
        'eonasdan-bootstrap-datetimepicker',
        'bootstrap-datepicker'
    ])
    .sass('resources/sass/paper-kit.scss', 'public/css/app.css');