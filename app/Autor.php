<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Autor extends Model
{
    //
	protected $table = 'autores';
	protected $primaryKey = 'id_autor';
	protected $fillable = ['id_registro','inicial_autor','inicial_apellido','afilicacion','ciudad_autor','pais_contacto'];
	protected $connection = ''; 
	public $timestamps = false;


   public function forms(){

   	return $this->belonsto('App\Form','id_registro');

   }
}
