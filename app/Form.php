<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Form extends Model

{
   protected $table = 'equipo_desafio';
   protected $primaryKey = 'id';
   protected $fillable = ['horario', 'nombre_equipo', 'categoria'];


     public function horas()
   {
       return $this->belongsTo('App\Hora','horario');
   }

    public function participantes()
    {
        return $this->hasMany( Participante::class, 'id_equipo', 'id' );
    }
}



