<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pais extends Model
{
    //
	protected $table = 'pais';
	protected $primaryKey = 'Id_Pais';
	protected $fillable = ['Nombre_Pais'];
	protected $connection = ''; 
	public $timestamps = false;


   public function forms(){

   	return $this->hasMany('App\Form','id_pais');

   }
}
