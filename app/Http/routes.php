<?php

session_start();

/*

|--------------------------------------------------------------------------

| Routes File

|--------------------------------------------------------------------------

|

| Here is where you will register all of the routes in an application.

| It's a breeze. Simply tell Laravel the URIs it should respond to

| and give it the controller to call when that URI is requested.

|

*/

Route::get('/personas', '\Idrd\Usuarios\Controllers\PersonaController@index');

Route::get('/personas/service/obtener/{id}', '\Idrd\Usuarios\Controllers\PersonaController@obtener');

Route::get('/personas/service/buscar/{key}', '\Idrd\Usuarios\Controllers\PersonaController@buscar');

Route::get('/personas/service/ciudad/{id_pais}', '\Idrd\Usuarios\Controllers\LocalizacionController@buscarCiudades');

Route::post('/personas/service/procesar/', '\Idrd\Usuarios\Controllers\PersonaController@procesar');



Route::any('/', 'MainController@index');
Route::get('pdf/{id}', 'PdfController@pdf')->name('to_print');
Route::post('comprobante', 'PdfController@document');

Route::any('/logout', 'MainController@logout');



//rutas con filtro de autenticación

Route::group(['middleware' => ['web']], function () {

Route::get('/welcome', 'MainController@welcome');
Route::get('login', function () {                



    return view('login');



});



Route::any('logear','MainController@logear');

Route::post('insertar', 'FormController@insertar');

Route::post('validar', 'MainController@validar');


Route::post('validar_categoria', 'FormController@validar_categoria');


Route::any('listar_datos', 'MainController@listar_datos')->name('index.lista.datos');

Route::get('/personas/service/ciudad/{id_pais}', '\Idrd\Usuarios\Controllers\LocalizacionController@buscarCiudades');

Route::post('/personas/service/procesar/', '\Idrd\Usuarios\Controllers\PersonaController@procesar');



});

/*