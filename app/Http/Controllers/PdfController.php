<?php

namespace App\Http\Controllers;

use App\Categoria;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB as DB;
use Validator;
use Session;
use App\Form;
use App\Equipo;

class PdfController extends Controller
{
    public function pdf($id)
    {

        if ( isset( $id ) ) {
            $formulario = Form::findOrFail( $id );
            $categoria = Categoria::findOrFail( $formulario->categoria );
            $view = view('pdf', ['formulario' => $formulario, 'categoria' => $categoria])->render();
            $pdf = \PDF::loadHTML($view);
            return $pdf->setPaper('a5', 'landscape')->stream('Comprobante');
        } else {
            return view('error', ['error' => 'No se encuentran registros para este usuario.']);
        }

    }
    public function document(Request $request)
    {

        if ( $request->has( 'cedula' ) ) {
            $formulario = Form::where('cedula', $request->get('cedula') )->firstOrFail();
            $categoria = Categoria::findOrFail( $formulario->categoria );
            $view = view('pdf', ['formulario' => $formulario, 'categoria' => $categoria])->render();
            $pdf = \PDF::loadHTML($view);
            return $pdf->setPaper('a5', 'landscape')->stream('Comprobante');
        } else {
            return view('error', ['error' => 'No se encuentran registros para este usuario.']);
        }

    }
}
