<?php

namespace App\Http\Controllers;
use App\Participante;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\DB as DB;
use Redirect;
use Validator;
use Session;
use App\Form;
use App\Pais;
use Idrd\Usuarios\Repo\Departamento;
use Idrd\Usuarios\Repo\Ciudad;
use App\Localidad;
use App\Hora;
use Idrd\Usuarios\Repo\Acceso;
use Mail;

class FormController extends BaseController


{
    var $url;

  
    private function cifrar($M)
    {   
      $C="";
      $k = 18; 
      for($i=0; $i<strlen($M); $i++)$C.=chr((ord($M[$i])+$k)%255);
      return $C;
    }

    private function decifrar($C)
    {   
      $M="";
      $k = 18;
      for($i=0; $i<strlen($C); $i++)$M.=chr((ord($C[$i])-$k+255)%255);
      return $M;
    }

    public function listar_datos()
    {

        $participantes = Participante::with('equipo')->all();

    $tabla='<strong><table id="lista" style="color: #FFFFFF">

        <thead>
           <tr>
             <th style="text-transform: capitalize;">Código de Inscripción</th>
             <th style="text-transform: capitalize;">Nombre del Equipo</th>
             <th style="text-transform: capitalize;">Categoróa</th>
             <th style="text-transform: capitalize;">Horario</th>
             <th style="text-transform: capitalize;">Nombres</th>
             <th style="text-transform: capitalize;">Apellidos</th>
             <th style="text-transform: capitalize;">Genero</th>
             <th style="text-transform: capitalize;">Fecha_nacimiento</th>
             <th style="text-transform: capitalize;">Mail</th>
             <th style="text-transform: capitalize;">Celular</th>
             <th style="text-transform: capitalize;">EPS</th>
             <th style="text-transform: capitalize;">Nombre contacto emergencia</th>   
             <th style="text-transform: capitalize;">Número de contacto de emergencia</th>
             <th style="text-transform: capitalize;">Fecha de Inscripción</th>             
            </tr>
        </thead>
        <tbody id="tabla"></strong>';

      foreach ($participantes as $key => $value)
      {
          $hora = Hora::find( isset( $value->equipo->hora ) ? $value->equipo->hora : 0 );

       $tabla.='<tr style="color: #000000"><td>'.isset( $value->id_equipo ) ? $value->id_equipo : 0 .'</td>';
       $tabla.='<td>'. isset( $value->equipo->nombre_equipo ) ? $value->equipo->nombre_equipo : 0 .'</td>';
       $tabla.='<td>'.isset( $value->equipo->categoria ) ? $value->equipo->categoria : 0 .'</td>';
       $tabla.='<td>'.isset( $hora->hora ) ? $hora->hora : 0 .'</td>';
       $tabla.='<td>'. isset( $value->nombres ) ? $value->nombres : null .'</td>';
       $tabla.='<td>'. isset( $value->apellidos ) ? $value->apellidos : null .'</td>';
       $tabla.='<td>'. isset( $value->genero ) ? $value->genero : null .'</td>';
       $tabla.='<td>'. isset( $value->fecha_nacimiento ) ? $value->fecha_nacimiento : null .'</td>';
       $tabla.='<td>'. isset( $value->mail ) ? $value->mail : null .'</td>';
       $tabla.='<td>'. isset( $value->celular ) ? $value->celular : null .'</td>';
       $tabla.='<td>'. isset( $value->eps ) ? $value->eps : null .'</td>';
       $tabla.='<td>'. isset( $value->nombre_contacto ) ? $value->nombre_contacto : null .'</td>';
       $tabla.='<td>'. isset( $value->numero_contacto ) ? $value->numero_contacto : null .'</td>';
       $tabla.='<td>'. isset( $value->created_at ) ? $value->created_at : null .'</td></tr>';

      }

      $tabla.='</tbody></table>';
      echo $tabla;
    }

      public function logear(Request $request)
      {

      $usuario = $request->input('usuario');
      $pass = $request->input('pass');
      $acceso = Acceso::where('Usuario',$usuario)->where('Contrasena', sha1($this->cifrar($pass)) )->first();
      if (empty($usuario)) { return view('error',['error' => 'Usuario o contraseña invalida!'] ); exit(); }
      if (empty($acceso)) { return view('error',['error' => 'Usuario o contraseña invalida!'] ); exit(); }

      session_start() ;
      
      $_SESSION['id_usuario'] = json_encode($acceso);
      return view('tabla'); exit();
      }

    public function insertar(Request $request)
    {
        $post = $request->input();


        if ( !$request->has('autores') || !is_array( $request->get('autores') ) ||  count($request->get('autores')) < 4 || count($request->get('autores')) > 4 ) {
            return view('error', ['error' => 'Se deben registrar 4 participantes entre ellos mínimo un hombre y una mujer.']);
        }


        if ( !$request->get('horario') ) {
            return view('error', ['error' => 'Se debe seleccionar un horario.']);
        }

        $m = 0;
        $f = 0;
        foreach ($request->get('autores') as $p) {
            if ($p['genero'] == 'Masculino') {
                $m++;
            }

            if ($p['genero'] == 'Femenino') {
                $f++;
            }
        }

        if ($m == 0) {
            return view('error', ['error' => 'Se debe registar al menos un hombre.']);
        }

        if ($f == 0) {
            return view('error', ['error' => 'Se debe registrar al menos una mujer.']);
        }


        $array = array_pluck($request->get('autores'), 'cedula');

        if (Participante::query()->whereIn('cedula', $array)->count() > 0) {
            return view('error', ['error' => 'Uno de los participantes o todos los participantes ya se encuentran registrados.']);
        }


        $formulario = new Form;

        //envio de correo

        if ($this->inscritos($request->get('horario')) < 3) {

            $formulario = $this->store($formulario, $request);
            $participantes = Participante::query()->where('id_equipo', $formulario->id)->get();
            $hora = Hora::findOrFail($formulario->horario);

            foreach ( $request->get('autores') as $participante ) {
                if (filter_var($participante['mail'], FILTER_VALIDATE_EMAIL)) {
                    Mail::send('email', ['formulario' => $formulario, 'hora' => $hora, 'participantes' => $participantes], function ($m) use ( $participante ) {
                        $m->from('no-reply@idrd.gov.co', 'Registro Exitoso al evento EL GRAN RETO DE GUERREROS');
                        $m->to( $participante['mail'],  "{$participante['nombres']} {$participante['apellidos']}")->subject('Registro Exitoso al evento EL GRAN RETO DE GUERREROS');
                    });
                }
            }


        } else {
            return view('error', ['error' => 'Lo sentimos el limite de inscritos fue superado!']);
        }


        return view('error', ['error' => '  BIENVENIDO, YA HACES PARTE DEL GRAN RETO DE GUERREROS.']);


    }


 // conteo de la tabla
    private function inscritos($horario)
    {

      $cant = Form::where('horario',$horario)->count('id');
      return $cant;
    }

    private function store($formulario, $input)
    {
        $formulario['nombre_equipo'] = $input['equipo'];
        $formulario['categoria'] = 'MIXTO';
        $formulario['horario'] = $input['horario'];
        $formulario->save();
        $formulario->participantes()->createMany( $input['autores'] );
        return $formulario;        
    }

}

