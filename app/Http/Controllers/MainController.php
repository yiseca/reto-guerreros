<?php 



namespace App\Http\Controllers;



use App\Http\Requests;

use App\Http\Controllers\Controller;

use App\Participante;
use Idrd\Usuarios\Repo\PersonaInterface;

use Illuminate\Http\Request;

use App\Form;

use App\Localidad;

use App\Hora;

use Idrd\Usuarios\Repo\Acceso;

use Mail;



class MainController extends Controller {



  protected $Usuario;

  protected $repositorio_personas;



  public function __construct(PersonaInterface $repositorio_personas)

  {

    if (isset($_SESSION['Usuario']))

      $this->Usuario = $_SESSION['Usuario'];



    $this->repositorio_personas = $repositorio_personas;

  }



  public function welcome()
  {

       $completo = Form::query()->groupBy('horario')->havingRaw('COUNT(horario) > 2')->get()->pluck('horario')->toArray();
       $hora = Hora::whereNotIn('id', $completo)->get();
       $localidades = Localidad::all();

   return view('welcome',["horas"=>$hora, 'localidades' => $localidades]);

  }



    public function index(Request $request)

  {

    $fake_permissions = ['5144', '1'];

    //$fake_permissions = null;



    if ($request->has('vector_modulo') || $fake_permissions)

    { 

      $vector = $request->has('vector_modulo') ? urldecode($request->input('vector_modulo')) : $fake_permissions;

      $user_array = is_array($vector) ? $vector : unserialize($vector);

      $permissions_array = $user_array;



      $permisos = [

        'permiso1' => array_key_exists(1, $permissions_array) ? intval($permissions_array[1]) : 0

      ];



      $_SESSION['Usuario'] = $user_array;

            $persona = $this->repositorio_personas->obtener($_SESSION['Usuario'][0]);



      $_SESSION['Usuario']['Recreopersona'] = [];

      $_SESSION['Usuario']['Roles'] = [];

      $_SESSION['Usuario']['Persona'] = $persona;

      $_SESSION['Usuario']['Permisos'] = $permisos;

      $this->Usuario = $_SESSION['Usuario'];

    } else {

      if (!isset($_SESSION['Usuario']))

        $_SESSION['Usuario'] = '';

    }



    if ($_SESSION['Usuario'] == '')

      return redirect()->away('http://www.idrd.gov.co/SIM/Presentacion/');



    return redirect('/welcome');

  }



  public function logout()

  {

    $_SESSION['Usuario'] = '';

    Session::set('Usuario', ''); 



    return redirect()->to('/');

  }



   private function cifrar($M)



    {   



      $C="";



      $k = 18; 



      for($i=0; $i<strlen($M); $i++)$C.=chr((ord($M[$i])+$k)%255);



      return $C;



    }







    private function decifrar($C)



    {   



      $M="";



      $k = 18;



      for($i=0; $i<strlen($C); $i++)$M.=chr((ord($C[$i])-$k+255)%255);



      return $M;



    }




    public function listar_datos()
    {

        $participantes = Participante::with('equipo')->get();

        $tabla='<table id="fresh-table" class="table" data-show-export="true" data-locale="es-MX">
        <thead>
           <tr>
             <th style="text-transform: capitalize;">Código de Inscripción</th>
             <th style="text-transform: capitalize;">Nombre del Equipo</th>
             <th style="text-transform: capitalize;">Categoría</th>
             <th style="text-transform: capitalize;">Horario</th>
             <th style="text-transform: capitalize;">Nombres</th>
             <th style="text-transform: capitalize;">Apellidos</th>
             <th style="text-transform: capitalize;">Genero</th>
             <th style="text-transform: capitalize;">Fecha_nacimiento</th>
             <th style="text-transform: capitalize;">Mail</th>
             <th style="text-transform: capitalize;">Celular</th>
             <th style="text-transform: capitalize;">EPS</th>
             <th style="text-transform: capitalize;">Nombre contacto emergencia</th>   
             <th style="text-transform: capitalize;">Número de contacto de emergencia</th>
             <th style="text-transform: capitalize;">Fecha de Inscripción</th>             
           </tr>
        </thead>
        <tbody id="tabla">';

        foreach ($participantes as $key => $value)
        {
            $hora = Hora::where('id', isset( $value->equipo->hora ) ? $value->equipo->hora : 0 )->first();

            $tabla.='<tr style="color: #000000"><td>'. $value->id_equipo  .'</td>';
            $tabla.='<td>'. $value->equipo->nombre_equipo  .'</td>';
            $tabla.='<td>'.  $value->equipo->categoria  .'</td>';
            $tabla.='<td>'.  $value->genero  .'</td>';
            $tabla.='<td>'.  $value->nombres  .'</td>';
            $tabla.='<td>'.  $value->apellidos .'</td>';
            $tabla.='<td>'.  $value->genero  .'</td>';
            $tabla.='<td>'.  $value->fecha_nacimiento  .'</td>';
            $tabla.='<td>'.  $value->mail  .'</td>';
            $tabla.='<td>'.  $value->celular  .'</td>';
            $tabla.='<td>'.  $value->eps  .'</td>';
            $tabla.='<td>'.  $value->nombre_contacto  .'</td>';
            $tabla.='<td>'.  $value->numero_contacto  .'</td>';
            $tabla.='<td>'.  $value->created_at  .'</td></tr>';

        }

        $tabla.='</tbody></table>';
        return view('tabla', ['tabla' => $tabla]);
    }










public function logear(Request $request)



    {







      $usuario = $request->input('usuario');



      $pass = $request->input('pass');



      $acceso = Acceso::where('Usuario',$usuario)->where('Contrasena', sha1($this->cifrar($pass)) )->first();







      if (empty($usuario)) { return view('error',['error' => 'Usuario o contraseña invalida!'] ); exit(); }



      if (empty($acceso)) { return view('error',['error' => 'Usuario o contraseña invalida!'] ); exit(); }



       



      // session_start() ;



       $_SESSION['id_usuario'] = json_encode($acceso);







      return redirect()->route('index.lista.datos');



    }







//función para validar si la cédula ya fue censada



   public function validar(Request $request)

   {

     if ($request-> has('cedula')) {

          $usuario = Form::where('cedula', $request->input('cedula'))->first(); 

     if (!empty($usuario)) { return response()-> json(['error'=>'Usted ya está censado  ']); exit(); }

     }



   }



//función para insertar 

   public function insertar(Request $request)



    {



     $post = $request->input();

     $usuario = Form::where('cedula', $request->input('cedula'))->first(); 

     if (!empty($usuario)) { return view('error',['error' => 'Este usuario ya fue registrado!'] ); exit(); 

    }

     $formulario = new Form([]);



      //envio de correo



     if($this->inscritos()<=1500)



     {



        $formulario = $this->store($formulario, $request);



        //$this->store($formulario, $request->input());



      

       /* Mail::send('email', ['user' => $request->input('correo_contacto'),'formulario' => $formulario], function ($m) use ($request) 

        {

            $m->from('no-reply@idrd.gov.co', 'IDRD');

            $m->to($request->input('correo_contacto'), $request->input('primer_nombre'))->subject('Registro Exitoso a la 2ª CRONOESCALADA ALTO DE PATIOS - 2018');

        });*/



      }else{

        return view('error', ['error' => 'Lo sentimos el limite de inscritos fue superado!']);

      }

        return view('error', ['error' =>'Usuario Censado']);

    }







 // conteo de la tabla

    private function inscritos()

    {



      $cant = Form::count('id');

      return $cant+1;

    }



    private function store($formulario, $input)



    {

      //dd($input->all());



        $formulario['nombre'] = $input['nombre'];

        $formulario['apellido'] = $input['apellido'];

        $formulario['tipo_documento'] = $input['tipo_documento'];

        $formulario['cedula'] = $input['cedula'];

        $formulario['eps'] = $input['eps'];

        $formulario['genero'] = $input['genero'];

        $formulario['sexo'] = $input['sexo'];

        $formulario['mail'] = $input['mail'];

        $formulario['ocupacion'] = $input['ocupacion'];

        $formulario['fecha_nacimiento'] = $input['fecha_nacimiento'];

        $formulario['edad'] = $input['edad'];

        $formulario['localidad'] = $input['localidad'];

        $formulario['escenario'] = $input['escenario'];

        $formulario['otro'] = $input['otro'];

        $formulario['nivel_estudio'] = $input['nivel_estudio'];

        $formulario['deporte'] = $input['deporte'];

        $formulario['actividad'] = $input['actividad'];

        $formulario['nivel_deportivo'] = $input['nivel_deportivo'];



        $formulario->save();

       

        return $formulario;        

    }

}