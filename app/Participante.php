<?php


namespace App;


use Illuminate\Database\Eloquent\Model;

class Participante extends Model
{
    protected $table = 'participantes_desafio';

    protected $fillable = [
        'id_equipo',
        'cedula',
        'tipo_documento',
        'nombres',
        'apellidos',
        'genero',
        'fecha_nacimiento',
        'mail',
        'celular',
        'eps',
        'nombre_contacto',
        'numero_contacto',
    ];

    public function equipo()
    {
        return $this->belongsTo(Form::class, 'id_equipo');
    }
}