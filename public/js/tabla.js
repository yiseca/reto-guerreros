 $().ready(function(){

     var $table = $('#fresh-table');



    $table.bootstrapTable({

        locale: 'es-MX',

        exportTypes: ['csv', 'excel', 'pdf'],

        exportDataType: 'all',

        showRefresh: false,

        pagination: true,

        search: true,

        showToggle: false,

        showColumns: false,

        sortable: true,

        striped: true,

        pageSize: 5,

        pageList: [5,10,25,50,100]

    });



    $(window).resize(function () {

        $table.bootstrapTable('resetView');

    });

   

});