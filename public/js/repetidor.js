$(document).ready(function () {
  $('.repeater').repeater({
    // (Optional)
    // start with an empty list of repeaters. Set your first (and only)
    // "data-repeater-item" with style="display:none;" and pass the
    // following configuration flag
    initEmpty: true,
    
    // (Optional)
    // "show" is called just after an item is added.  The item is hidden
    // at this point.  If a show callback is not given the item will
    // have $(this).show() called on it.
    show: function () {
      var limitcount = $(this).parents(".repeater").data("limit");
      var itemcount = $(this).parents(".repeater").find("div[data-repeater-item]").length;
      
      if (limitcount) {
        if (itemcount <= limitcount) {
          $(this).slideDown();
        } else {
          $(this).remove();
        }
      } else {
        $(this).slideDown();
      }
  
      if (itemcount >= limitcount) {
        $("button[data-repeater-create]").hide("slow");
      }
    },
    hide: function (deleteElement) {
      var limitcount = $(this).parents(".repeater").data("limit");
      var itemcount = $(this).parents(".repeater").find("div[data-repeater-item]").length;
      
      $(this).slideUp(deleteElement);
    
      if (itemcount <= limitcount) {
        $("button[data-repeater-create]").show("slow");
      }
    },
  
  })

  

 var isValid = false;
 
  
  $('#btn_send').on('click', function (e) {
    
    if ( !isValid ) {
      e.preventDefault();
      var $data = $('.repeater').repeaterVal();
  
  
      if ( $data.hasOwnProperty('autores') ) {
        if ( $data.autores.length == 4) {
          var m = 0, f = 0;
          $data.autores.map( p => {
            if (p.genero == 'Masculino') {
              m++;
            }
            if (p.genero == 'Femenino') {
              f++;
            }
          })
      
          if ( m == 0 ) {
            $('#error_message').html(
              '<div class="alert alert-danger alert-dismissible fade show" role="alert">\n' +
              '  Se debe registrar al menos un hombre\n' +
              '  <button type="button" class="close" data-dismiss="alert" aria-label="Close">\n' +
              '    <span aria-hidden="true">&times;</span>\n' +
              '  </button>\n' +
              '</div>'
            )
        
            return;
          }
      
          if ( f == 0 ) {
            $('#error_message').html(
              '<div class="alert alert-danger alert-dismissible fade show" role="alert">\n' +
              '  Se debe registrar al menos una mujer\n' +
              '  <button type="button" class="close" data-dismiss="alert" aria-label="Close">\n' +
              '    <span aria-hidden="true">&times;</span>\n' +
              '  </button>\n' +
              '</div>'
            )
        
            return;
          }
      
          isValid = true;
          $('#form_gen').submit();
      
        }
        else if ( $data.autores.length < 4 || $data.autores.length > 4 ) {
          $('#error_message').html(
            '<div class="alert alert-danger alert-dismissible fade show" role="alert">\n' +
            '  Se deben registrar 4 participantes mínimo un hombre y una mujer\n' +
            '  <button type="button" class="close" data-dismiss="alert" aria-label="Close">\n' +
            '    <span aria-hidden="true">&times;</span>\n' +
            '  </button>\n' +
            '</div>'
          )
        }
      } else {
        $('#error_message').html(
          '<div class="alert alert-danger alert-dismissible fade show" role="alert">\n' +
          '  Se deben registrar 4 participantes mínimo un hombre y una mujer\n' +
          '  <button type="button" class="close" data-dismiss="alert" aria-label="Close">\n' +
          '    <span aria-hidden="true">&times;</span>\n' +
          '  </button>\n' +
          '</div>'
        )
      }
    } else {
      $('#form_gen').submit();
    }
    
  })

});